*points at pedestrian* *pedestrian takes out grenade* *pedestrian pulls out pin* *pedestrian blows up*
Yes, that is my description of the mod. :P

How to install:
1. Install .NET Scripthook.
2. Copy PedSuicideBomber.net.dll and PedSuicideBomber.ini to the scripts folder in your GTA IV directory.

Controls:
Aim at ped then press the middle mouse button = Make ped kill himself with a grenade

Changeable in the ini file:
The controls.
The explosive force.

Credits:
xboxlover5000: for requesting the script
LetsPlayOrDy (now named Jitnaught): for creating the script

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
