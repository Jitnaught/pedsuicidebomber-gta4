﻿using GTA;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace PedSuicideBomber
{
    public class PedSuicideBomber : Script
    {
        enum cAnimationFlags
        {
            None = 0,
            DontComeBackToInitialPosition = 1,
            ReturnToInitialPosition = 8,
            Loop = 16,
            FreezePedAtLastAnimPosition = 32,
            ResetToStandingIdle = 64,
            UpperBodyOnly = 256,
            NoSound = 512,
            UpperBodyOnlyAndAbilityToWalk = 1024 | UpperBodyOnly
        }

        AnimationSet animSet = new AnimationSet("throw_grenade");
        string animName = "aim_partial";

        Keys pressKey, holdKey;
        bool useHoldKey;
        float explosionPowerValue;

        List<Ped> suicideBombers = new List<Ped>();

        public PedSuicideBomber()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue("ped_suicide_press_key", "keys", Keys.MButton);
                Settings.SetValue("ped_suicide_hold_key", "keys", Keys.RControlKey);
                Settings.SetValue("use_ped_suicide_hold_key", "keys", false);
                Settings.SetValue("explosion_power", "other", 1);
                Settings.Save();
            }

            pressKey = Settings.GetValueKey("ped_suicide_press_key", "keys", Keys.MButton);
            holdKey = Settings.GetValueKey("ped_suicide_hold_key", "keys", Keys.RControlKey);
            useHoldKey = Settings.GetValueBool("use_ped_suicide_hold_key", "keys", false);
            explosionPowerValue = Settings.GetValueFloat("explosion_power", "other", 1);

            KeyDown += PedSuicideBomber_KeyDown;
            Interval = 100;
            Tick += PedSuicideBomber_Tick;
        }

        void PedSuicideBomber_Tick(object sender, EventArgs e)
        {
            try
            {
                if (suicideBombers.Count > 0)
                {
                    Ped[] saved = suicideBombers.ToArray();

                    foreach (Ped p in saved)
                    {
                        if (p != null && p.isAlive && p.Animation.isPlaying(animSet, animName))
                        {
                            if (Game.GameTime - p.Metadata.StartTime >= 5000)
                            {
                                Vector3 bonePosition = p.GetBonePosition(Bone.RightHand);
                                p.BlockGestures = false;
                                p.BlockPermanentEvents = false;
                                p.BlockWeaponSwitching = false;
                                if (p.Weapons.Grenades.AmmoInClip == 1)
                                {
                                    p.Weapons.Select(Weapon.None);
                                    p.Weapons.Grenades.AmmoInClip = 0;
                                }
                                World.AddExplosion(bonePosition, ExplosionType.Default, explosionPowerValue);
                                suicideBombers.Remove(p);
                            }
                        }
                        else
                        {
                            suicideBombers.Remove(p);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Game.Console.Print("Ped Suicide Bomber: Error: " + ex.ToString());
                Game.DisplayText("Error: " + ex.ToString(), 3000);
            }
        }

        private void PedSuicideBomber_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if ((!useHoldKey || isKeyPressed(holdKey)) && e.Key == pressKey)
            {
                Ped target = null;
                if ((target = Player.GetTargetedPed()) != null && target.isAlive && !suicideBombers.Contains(target))
                {
                    try
                    {
                        if (target.isInVehicle())
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                if (!target.isInVehicle()) break;
                                else
                                {
                                    target.Task.LeaveVehicle(target.CurrentVehicle, true);
                                    Wait(500);
                                }
                            }
                        }

                        target.BecomeMissionCharacter();
                        target.BlockGestures = true;
                        target.BlockPermanentEvents = true;
                        target.BlockWeaponSwitching = true;
                        target.Task.ClearAllImmediately();
                        target.Weapons.Select(Weapon.Thrown_Grenade);
                        if (target.Weapons.Current.AmmoInClip <= 0) target.Weapons.Current.AmmoInClip = 1;

                        target.Animation.Play(animSet, animName, 8f, (AnimationFlags)(cAnimationFlags.Loop | cAnimationFlags.UpperBodyOnlyAndAbilityToWalk));

                        for (int i = 0; i < 10; i++)
                        {
                            if (target.Animation.isPlaying(animSet, animName)) break;
                            else target.Animation.Play(animSet, animName, 8f, (AnimationFlags)(cAnimationFlags.Loop | cAnimationFlags.UpperBodyOnlyAndAbilityToWalk));
                            Wait(200);
                        }

                        if (target.Animation.isPlaying(animSet, animName))
                        {
                            target.Metadata.StartTime = Game.GameTime;

                            suicideBombers.Add(target);
                        }
                        else
                        {
                            target.BlockGestures = false;
                            target.BlockPermanentEvents = false;
                            target.BlockWeaponSwitching = false;
                            target.Weapons.Select(Weapon.None);
                        }
                    }
                    catch (Exception ex)
                    {
                        Game.Console.Print("Ped Suicide Bomber: Error: " + ex.ToString());
                        Game.DisplayText("Error: " + ex.ToString(), 3000);
                    }
                }
            }
        }
    }
}
